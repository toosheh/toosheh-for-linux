<img src = "resource/ui/about-toosheh.png" style="align:center;"></img>
<h1 style="text-align:center">Unofficial Toosheh client for Linux written in C++ and gtkmm (GNOME Toolkit for C++)</h1>

<h2>install</h2>
<h3>AUR (Manjaro, Arch, Endeavour OS and Garuda Linux)</h3>


You can use any AUR helper such as yay: 

`yay -S toosheh`

you can also clone the <a href="https://aur.archlinux.org/toosheh.git" title="Toosheh Repo">AUR repo</a> and use `makepkg` and `pacman -U toosheh.tar.zst` to build and install it from PKGBUID.

<h3>AppImage</h3>

<a href="https://www.appimagehub.com/p/1506686/"><img src="AppImage/AppImageHub.png"></img></a>

<p>you can also install this software on any distro like: Debian, Ubuntu, Fedora, OpenSUSE, KDE Neon, Pop!OS and Linux Mint. make Toosheh AppImage executable and double click on it.</p>
<p>The best way to integrate your AppImages into your system is <a href="https://github.com/TheAssassin/AppImageLauncher/releases">AppImage Launcher</a>.</p>
