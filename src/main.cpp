//special: Extraction has been finished.
//read string retuns line: awk '/(Result)/{ print NR; exit }' log.txt

#include <gtkmm.h>
#include <iostream>


using namespace std;

Glib::RefPtr<Gtk::Application> app;//you can make this in main() but i made this here because i use this thing in onExitButtonClicked()
Glib::RefPtr<Gtk::Builder> builder;
Glib::RefPtr<Gtk::ProgressBar> ExtractProgress;
Glib::RefPtr<Gtk::Button> ExtractButton;
Glib::RefPtr<Gtk::Button> OpenFolderButton;


string PathOutput;

string commandReadString (string cmd)
{
    char buffer[128];
    string outputstr;
    FILE* pipe = popen(cmd.c_str(), "r");
    while (fgets(buffer, sizeof buffer, pipe) != NULL) 
    {
        outputstr += buffer;
    }
    pclose(pipe);
    return outputstr;
}


bool progressCheck (void)
{
    string logfilestr = commandReadString("tail -1 " + PathOutput + "/log.txt");//read the last line of the logfile
    auto special_string_cspp_location = logfilestr.find(", (%"); //cspp stands for   , (%   comma, space, parantesis, precent
    static int precent;
    if(special_string_cspp_location != string::npos)//if we found the precent number
    {
        logfilestr.erase(0,special_string_cspp_location + 4);// clear everything before paranthesis and precent and also paranthesis and precent itself
        precent = stoi(logfilestr);
        ExtractProgress -> set_fraction (float(precent)/100.0);
    }
    
    //cout << "InAppPrecent: " << precent << endl;
    if (precent > 98 || (special_string_cspp_location == string::npos))
    {
        //cout << "near-end detected with: " << logfilestr << endl;

        string finishstr = "Extraction has been finished.";
        if (logfilestr.find(finishstr) != string::npos)
        {
            ExtractProgress -> set_fraction(1.0);
            
            //cout << "compeleted!" << endl;
            string result = commandReadString("tail -22 " + PathOutput + "/log.txt");
            OpenFolderButton->set_sensitive(true);
            auto messageType = Gtk::MESSAGE_INFO;
            string ResultTitle = "Result";
            if(result.find("Could not find") != string::npos)
            {
                messageType = Gtk::MESSAGE_ERROR;
                ResultTitle = "Couldn't Find Any Ts File.";
                //cout << "error detected" << endl;
            } 
            Gtk::MessageDialog ResultDialog (ResultTitle, false, messageType, Gtk::BUTTONS_OK);
            ResultDialog.set_secondary_text(result);
            ResultDialog.run();
            ExtractButton -> set_sensitive(true);
            return false; //deactivate the timer
        }
    }
    return true;//hell yeah keep pushing!
}


void onExtractButtonClicked (void)
{
    //system("nohup cmd > /dev/null 2>&1 &"); //commad to make things async
    auto deleteCheckBox = Glib::RefPtr<Gtk::CheckButton>::cast_dynamic(builder->get_object("Delete"));
    ExtractButton -> set_sensitive(false);
    auto InputFolderButton = Glib::RefPtr<Gtk::FileChooserButton>::cast_dynamic(builder->get_object("Input"));
    auto OutputFolderButton = Glib::RefPtr<Gtk::FileChooserButton>::cast_dynamic(builder->get_object("Output"));
    string PathInput = InputFolderButton->get_file()->get_path();
    PathOutput = OutputFolderButton->get_file()->get_path();
    PathInput = '\'' + PathInput + '\'';
    PathOutput = '\'' + PathOutput + '\'';
    string delstr = " ";
    if (deleteCheckBox->get_active())  delstr = " /delete";
    //system(string("rm " + PathOutput + "//log.txt").c_str());//remove previous log file if it exised for making sure that prossessCheck function will work properly....
    string cmd = "nohup toosheh-extractor " + PathOutput + " /ts " + PathInput + delstr + " > /dev/null 2>&1 &";
    usleep(200000);//we wait for 200ms (it does not hurt performance because another thread handles the extracting process...)
    //cout << cmd << endl;
    system(cmd.c_str());
    Glib::signal_timeout().connect(sigc::ptr_fun(&progressCheck),100);
}

void onFolderButtonClicked (void)
{
    string cmdfolder = "xdg-open " + PathOutput;
    system (cmdfolder.c_str());
}


void onExitButtonClicked (void)
{
    app->quit();
}

void onAboutClicked (void)
{
    auto myAboutDialog = Glib::RefPtr<Gtk::AboutDialog>::cast_dynamic(builder->get_object("About")); 
    myAboutDialog->run();
    myAboutDialog->hide(); //myAboutDialog will be deleted after exiting from this function so there is no need to worry...
}

bool IsInputAvailable = false; 
bool IsOutputAvailable = false;
void makeExtractWork ()
{
    if(IsInputAvailable && IsOutputAvailable) ExtractButton ->set_sensitive(true);
}

void inputFolderCheck (void)
{
    IsInputAvailable=true;
    makeExtractWork();
}

void outputFolderCheck(void)
{   
    IsOutputAvailable=true;
    makeExtractWork();
}




int main (int argc,char* argv[])
{
    app = Gtk::Application::create(argc,argv,"org.master81.toosheh");
    builder = Gtk::Builder::create_from_resource("/main/ui/Toosheh.glade");
    auto ObjectWindow = builder->get_object("MainWindow");
    Glib::RefPtr<Gtk::ApplicationWindow> MainWindow = Glib::RefPtr<Gtk::ApplicationWindow>::cast_dynamic(ObjectWindow);
    
    auto InputFolderButton = Glib::RefPtr<Gtk::FileChooserButton>::cast_dynamic(builder->get_object("Input"));
    auto OutputFolderButton = Glib::RefPtr<Gtk::FileChooserButton>::cast_dynamic(builder->get_object("Output"));
    InputFolderButton -> signal_file_set().connect(sigc::ptr_fun(&inputFolderCheck));
    OutputFolderButton -> signal_file_set().connect(sigc::ptr_fun(&outputFolderCheck));

    auto ExitButton = Glib::RefPtr<Gtk::Button>::cast_dynamic(builder -> get_object ("ExitButton"));
    ExitButton->signal_clicked().connect(sigc::ptr_fun(&onExitButtonClicked));

    auto AboutButton = Glib::RefPtr<Gtk::Button>::cast_dynamic(builder->get_object("AboutButton"));
    AboutButton->signal_clicked().connect(sigc::ptr_fun(&onAboutClicked));

    ExtractProgress = Glib::RefPtr<Gtk::ProgressBar>::cast_dynamic(builder->get_object("Progress"));
    
    auto ObjectExtractButton = builder -> get_object("Submit");
    ExtractButton = Glib::RefPtr<Gtk::Button>::cast_dynamic(ObjectExtractButton);
    ExtractButton -> signal_clicked().connect(sigc::ptr_fun(&onExtractButtonClicked));

    OpenFolderButton = Glib::RefPtr<Gtk::Button>::cast_dynamic(builder->get_object("FolderBtn"));
    OpenFolderButton -> signal_clicked().connect(sigc::ptr_fun(&onFolderButtonClicked));
    
    app -> run (*(MainWindow.get()));
}
