#!/bin/bash
#you need gcc gtkmm-3.0 glib-compile-resources and appimagetool if you want to build this app from source
echo compiling main application
cd resource
glib-compile-resources --target=resources.c --generate-source resources.xml
cd ..
g++ -rdynamic src/main.cpp resource/resources.c -o  toosheh `pkg-config gtkmm-3.0 --libs --cflags`
echo "done"
echo building AppImage
cp toosheh AppImage/AppDir/toosheh
cd AppImage
appimagetool AppDir
echo "done"
